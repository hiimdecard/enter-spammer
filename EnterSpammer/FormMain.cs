﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace EnterSpammer
{
    public partial class FormMain : Form
    {
        private const UInt32 WM_KEYDOWN = 0x0100;
        private const UInt32 WM_KEYUP = 0x0101;
        private const int VK_RETURN = 0x0D;

        private Process[] processlist;
        private Process myWow;
        [DllImport("user32.dll")]
        static extern bool PostMessage(IntPtr hWnd, UInt32 Msg, int wParam, int lParam);

        private IntPtr p;

        public FormMain()
        {
            InitializeComponent();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            processlist = Process.GetProcessesByName("Wow");
            comboBoxSelectProcess.DataSource = processlist;
            comboBoxSelectProcess.DisplayMember = "processName";
            textBoxInterval.Text = (((double) timerEnterSpam.Interval) / 1000).ToString();

        }

        private void buttonStart_Click(object sender, EventArgs e)
        {
            timerEnterSpam.Enabled = true;
            buttonStart.Enabled = false;
            buttonStop.Enabled = true;
            comboBoxSelectProcess.Enabled = false;
            textBoxInterval.Enabled = false;
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            timerEnterSpam.Enabled = false;
            buttonStart.Enabled = true;
            buttonStop.Enabled = false;
            comboBoxSelectProcess.Enabled = true;
            textBoxInterval.Enabled = true;
        }

        private void timerEnterSpam_Tick(object sender, EventArgs e)
        {
            PostMessage(myWow.MainWindowHandle, WM_KEYDOWN, VK_RETURN, 0);
            PostMessage(myWow.MainWindowHandle, WM_KEYUP, VK_RETURN, 0);
        }

        private void comboBoxSelectProcess_SelectedIndexChanged(object sender, EventArgs e)
        {
            myWow = processlist[comboBoxSelectProcess.SelectedIndex];
            p = myWow.MainWindowHandle;
        }

        private void textBoxInterval_TextChanged(object sender, EventArgs e)
        {
            if (Double.TryParse(textBoxInterval.Text, out double result));
            timerEnterSpam.Interval = (int) (1000 * result);
        }
    }
}
