﻿namespace EnterSpammer
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelSelectProcess = new System.Windows.Forms.Label();
            this.comboBoxSelectProcess = new System.Windows.Forms.ComboBox();
            this.timerEnterSpam = new System.Windows.Forms.Timer(this.components);
            this.buttonStart = new System.Windows.Forms.Button();
            this.buttonStop = new System.Windows.Forms.Button();
            this.labelInterval = new System.Windows.Forms.Label();
            this.textBoxInterval = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // labelSelectProcess
            // 
            this.labelSelectProcess.AutoSize = true;
            this.labelSelectProcess.Location = new System.Drawing.Point(12, 9);
            this.labelSelectProcess.Name = "labelSelectProcess";
            this.labelSelectProcess.Size = new System.Drawing.Size(81, 13);
            this.labelSelectProcess.TabIndex = 0;
            this.labelSelectProcess.Text = "Select Process:";
            // 
            // comboBoxSelectProcess
            // 
            this.comboBoxSelectProcess.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSelectProcess.FormattingEnabled = true;
            this.comboBoxSelectProcess.Location = new System.Drawing.Point(15, 25);
            this.comboBoxSelectProcess.Name = "comboBoxSelectProcess";
            this.comboBoxSelectProcess.Size = new System.Drawing.Size(156, 21);
            this.comboBoxSelectProcess.TabIndex = 1;
            this.comboBoxSelectProcess.SelectedIndexChanged += new System.EventHandler(this.comboBoxSelectProcess_SelectedIndexChanged);
            // 
            // timerEnterSpam
            // 
            this.timerEnterSpam.Interval = 500;
            this.timerEnterSpam.Tick += new System.EventHandler(this.timerEnterSpam_Tick);
            // 
            // buttonStart
            // 
            this.buttonStart.Location = new System.Drawing.Point(15, 72);
            this.buttonStart.Name = "buttonStart";
            this.buttonStart.Size = new System.Drawing.Size(75, 23);
            this.buttonStart.TabIndex = 2;
            this.buttonStart.Text = "Start";
            this.buttonStart.UseVisualStyleBackColor = true;
            this.buttonStart.Click += new System.EventHandler(this.buttonStart_Click);
            // 
            // buttonStop
            // 
            this.buttonStop.Enabled = false;
            this.buttonStop.Location = new System.Drawing.Point(96, 72);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 3;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // labelInterval
            // 
            this.labelInterval.AutoSize = true;
            this.labelInterval.Location = new System.Drawing.Point(12, 52);
            this.labelInterval.Name = "labelInterval";
            this.labelInterval.Size = new System.Drawing.Size(59, 13);
            this.labelInterval.TabIndex = 4;
            this.labelInterval.Text = "Interval (s):";
            // 
            // textBoxInterval
            // 
            this.textBoxInterval.Location = new System.Drawing.Point(71, 49);
            this.textBoxInterval.Name = "textBoxInterval";
            this.textBoxInterval.Size = new System.Drawing.Size(100, 20);
            this.textBoxInterval.TabIndex = 5;
            this.textBoxInterval.TextChanged += new System.EventHandler(this.textBoxInterval_TextChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(186, 109);
            this.Controls.Add(this.textBoxInterval);
            this.Controls.Add(this.labelInterval);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.buttonStart);
            this.Controls.Add(this.comboBoxSelectProcess);
            this.Controls.Add(this.labelSelectProcess);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "FormMain";
            this.Text = "Enter Spammer";
            this.Load += new System.EventHandler(this.FormMain_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelSelectProcess;
        private System.Windows.Forms.ComboBox comboBoxSelectProcess;
        private System.Windows.Forms.Timer timerEnterSpam;
        private System.Windows.Forms.Button buttonStart;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Label labelInterval;
        private System.Windows.Forms.TextBox textBoxInterval;
    }
}

